var exec = require('cordova/exec');

var PLUGIN_NAME = 'TdiPlugin';

function TdiPlugin() {
}

TdiPlugin.prototype.initFaceCapture = function(callback, errorCallback) {
 exec(callback, errorCallback, PLUGIN_NAME, 'initFaceCapture', []);
                                         };
TdiPlugin.prototype.init = function(callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'init', []);
                                         };

TdiPlugin.prototype.setDetectionZone = function(var1, var2 , callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setDetectionZone', [var1, var2]);
                                         };

TdiPlugin.prototype.setCaptureDocuments = function(docChoice, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setCaptureDocuments', [docChoice]);
                                         };

TdiPlugin.prototype.setCaptureNewDocument = function(width, height, pages, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setCaptureNewDocument', [width, height, pages]);
                                         };

TdiPlugin.prototype.setAutoSnapshot = function(autoSnapshot, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setAutoSnapshot', [autoSnapshot]);
                                         };

TdiPlugin.prototype.setEdgeDetectionTimeout  = function(timeout, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setEdgeDetectionTimeout', [timeout]);
                                         };

TdiPlugin.prototype.setAutocropping = function(autocropping, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setAutocropping', [autocropping]);
                                         };

TdiPlugin.prototype.hideUIElementsForStep = function(step, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'hideUIElementsForStep', [step]);
                                         };
TdiPlugin.prototype.setQACheckResultTimeout = function(timeout, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setQACheckResultTimeout', [timeout]);
                                         };
TdiPlugin.prototype.setSecondPageTimeout = function(timeout, callback, errorCallback) {
                                         exec(callback, errorCallback, PLUGIN_NAME, 'setSecondPageTimeout', [timeout]);
                                         };

TdiPlugin.prototype.setTDIConfig = function(SCENARIO_NAME, BASE_URL, JWT_TOKEN, TenantId, callback, errorCallback) {
exec(callback, errorCallback, PLUGIN_NAME, 'setTDIConfig', [SCENARIO_NAME, BASE_URL, JWT_TOKEN, TenantId]);
};

TdiPlugin.prototype.initTDI = function(callback, errorCallback) {
exec(callback, errorCallback, PLUGIN_NAME, 'initTDI', []);
};

TdiPlugin.prototype.setLivenessConfig = function(workflow,username, callback, errorCallback) {
exec(callback, errorCallback, PLUGIN_NAME, 'setLivenessConfig', [workflow,username]);
};

var TdiPlugin = new TdiPlugin();
module.exports = TdiPlugin;
