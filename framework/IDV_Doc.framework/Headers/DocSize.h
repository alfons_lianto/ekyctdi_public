//
//  DocSize.h
//  mcid_sdk
//
//  Created by Gemalto Juan on 1/2/18.
//  Copyright © 2018 Gemalto. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TD1_DOC_SIZE [[DocSize alloc] initWithWidth:  85.60   andHeight: 53.98]
#define TD2_DOC_SIZE [[DocSize alloc] initWithWidth: 105.00   andHeight: 74.00]
#define TD3_DOC_SIZE [[DocSize alloc] initWithWidth: 125.00   andHeight: 88.00]

@interface DocSize : NSObject {
    @public float width;
    @public float height;
}

-(id)initWithWidth:(float)p_width andHeight:(float)p_height __deprecated_msg("Use setCaptureDocuments instead."); // 12-09-2018
-(double)aspectRatio __deprecated_msg("Use setCaptureDocuments instead."); // 12-09-2018

@end
