var fsCallback = function (err) {
    if (err) {
        console.error("Failed to create directory or file.");
        throw err;
    }
}
module.exports = function(context) {
    var fs = require('fs');
	var fsEx = require("fs.extra");
    var path = require('path');
    var config_xml = path.join(context.opts.projectRoot, 'config.xml');
    var et = require('elementtree');

    var data = fs.readFileSync(config_xml).toString();
    var etree = et.parse(data);
    var app_id = etree.getroot().attrib.id;

    console.log("Before build js for Android");

    //Get Application id
    console.log(etree.getroot().attrib.id);

    var file = fs.readFileSync('plugins/cordova.plugin.tdi/src/android/CaptureDocActivity.java', 'utf8');
    var result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync('plugins/cordova.plugin.tdi/src/android/CaptureDocActivity.java', result);
    
    var file = fs.readFileSync('plugins/cordova.plugin.tdi/src/android/CaptureFaceActivity.java', 'utf8');
    var result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync('plugins/cordova.plugin.tdi/src/android/CaptureFaceActivity.java', result);
    
    var file = fs.readFileSync('plugins/cordova.plugin.tdi/src/android/LivenessFragment.java', 'utf8');
    var result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync('plugins/cordova.plugin.tdi/src/android/LivenessFragment.java', result);
    
    var file = fs.readFileSync('plugins/cordova.plugin.tdi/src/android/SplashActivity.java', 'utf8');
    var result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync('plugins/cordova.plugin.tdi/src/android/SplashActivity.java', result);
    

    console.log('Application id replacement is done!');

    // Copy CaptureDocActivity.java
    fsEx.copy('plugins/cordova.plugin.tdi/src/android/CaptureDocActivity.java', 'platforms/android/app/src/main/java/cordova/plugin/tdi/CaptureDocActivity.java', { replace: true }, fsCallback);
    
    fsEx.copy('plugins/cordova.plugin.tdi/src/android/CaptureFaceActivity.java', 'platforms/android/app/src/main/java/cordova/plugin/tdi/CaptureFaceActivity.java', { replace: true }, fsCallback);
    
    fsEx.copy('plugins/cordova.plugin.tdi/src/android/LivenessFeedbackView.java', 'platforms/android/app/src/main/java/cordova/plugin/tdi/LivenessFeedbackView.java', { replace: true }, fsCallback);
    
    fsEx.copy('plugins/cordova.plugin.tdi/src/android/LivenessFragment.java', 'platforms/android/app/src/main/java/cordova/plugin/tdi/LivenessFragment.java', { replace: true }, fsCallback);
    
    fsEx.copy('plugins/cordova.plugin.tdi/src/android/McidSDKIntegration.java', 'platforms/android/app/src/main/java/cordova/plugin/tdi/McidSDKIntegration.java', { replace: true }, fsCallback);
    
    fsEx.copy('plugins/cordova.plugin.tdi/src/android/SplashActivity.java', 'platforms/android/app/src/main/java/cordova/plugin/tdi/SplashActivity.java', { replace: true }, fsCallback);

    // Copy activity_capture_doc.xml to res/layout
    fsEx.mkdirp('platforms/android/app/src/main/res/layout', fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/activity_capture_doc.xml', 'platforms/android/app/src/main/res/layout/activity_capture_doc.xml', { replace: true }, fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/activity_capture_face.xml', 'platforms/android/app/src/main/res/layout/activity_capture_face.xml', { replace: true }, fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/activity_knomi_liveness.xml', 'platforms/android/app/src/main/res/layout/activity_knomi_liveness.xml', { replace: true }, fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/activity_liveness_custom.xml', 'platforms/android/app/src/main/res/layout/activity_liveness_custom.xml', { replace: true }, fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/fragment_knomi_liveness.xml', 'platforms/android/app/src/main/res/layout/fragment_knomi_liveness.xml', { replace: true }, fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/activity_main.xml', 'platforms/android/app/src/main/res/layout/activity_main.xml', { replace: true }, fsCallback);
    fsEx.copy('plugins/cordova.plugin.tdi/src/res/layout/activity_splash.xml', 'platforms/android/app/src/main/res/layout/activity_splash.xml', { replace: true }, fsCallback);

    console.log('Customization done for Android');

};
