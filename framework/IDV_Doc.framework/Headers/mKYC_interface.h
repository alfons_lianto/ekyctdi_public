//
//  mKYC_interface.h
//  mcid_sdk
//
//  Copyright © 2016 Gemalto. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "DocSize.h"

enum
{
    idDocument = 0,
    passport = 1,
    Custom = 2,
    Autodetect = 3
} __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018

typedef enum MKYCError : NSUInteger {
    MKYCError_RootedDevice,
    MKYCError_AppInBackground
} MKYCError __deprecated_msg("Use CaptureError instead."); // 31-09-2018;

typedef NSUInteger ECaptureMode;

typedef NSUInteger EEdgesDetectionMode;

@protocol BaseCaptureDelegate <NSObject>
@end

__attribute__ ((deprecated("use CaptureInterface and CaptureDelegate instead mKYC_interface and mKYC_interface")))
@protocol mKycDelegate <BaseCaptureDelegate>
- (void)captureDone:(NSData*)p_side1 side:(NSData*)p_side2 metadata:(NSDictionary*)p_metaData;
@optional
- (void)captureFailed:(MKYCError)p_failureCode;
@end


/**
 @deprecated SDK Interface
 */
@interface mKYC_interface : UIView
-(void) startMkycCaptureWithDelegate:(id<mKycDelegate>)p_delegate __deprecated_msg("Use init & start instead."); // 31-09-2018
-(void) startMkycCaptureWithDelegate:(id<mKycDelegate>)p_delegate withDetection: (bool)enableDetection __deprecated_msg("Use init & start instead."); // 31-09-2018
-(void) cancelCurrentCapture;
-(void) stop;
-(void) stopCapture __deprecated_msg("Use stop instead."); // 31-09-2018
-(void) deinitiallize __deprecated_msg("Use releaseMemory instead."); // 31-09-2018
-(void) releaseMem __deprecated_msg("Use releaseMemory instead."); // 31-09-2018;

-(void) setCaptureMode:(ECaptureMode)p_captureMode __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setCaptureMode:(ECaptureMode)p_captureMode andSides:(int)p_sides __deprecated_msg("use setNumberOfSides to set the number of sides instead."); // 16-04-2018
-(void) setCaptureSizesInMillimeters:(NSArray<DocSize *>*)p_sizes __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setNumberOfSides:(int)p_sides __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setAutocaptureEnabled:(Boolean)p_enable __deprecated_msg("Use setAutoSnapshot instead."); // 31-09-2018;
-(void) setDetectionZoneSpace:(int) width andAspectRatio:(double)ar;
-(void) setEdgeDetectionTimeout:(int)p_timeOutMsec __deprecated_msg("Stop using this method, is not working anymore.");
-(void) setEdgeDetectionMode:(EEdgesDetectionMode)p_mode;

-(void) setCroppingEnabled:(bool)enabled __deprecated_msg("Use setAutoCropping instead."); // 31-09-2018;
-(bool) isCroppingEnabled __deprecated_msg("Use setAutoCropping instead."); // 31-09-2018;

-(bool) isDetectionEnabled __deprecated_msg("Use init & start instead."); // 31-09-2018;
-(void) setDetectionEnabled: (bool)p_enable __deprecated_msg("Use init & start instead."); // 31-09-2018;

-(void) setQACheckResultTimeout: (int)timeoutSec;
-(int) getQACheckResultTimeout __deprecated_msg("This method will be removed");
-(void) setSecondPageTimeout: (int)timeoutSec;
-(int) getSecondPageTimeout __deprecated_msg("This method will be removed");

-(void) setFitDocumentDelay: (int)timeoutSec __deprecated_msg("Use detection warning delegate to receive events from detection process"); // 31-09-2018
-(int) getFitDocumentDelay                   __deprecated_msg("Use detection warning delegate to receive events from detection process"); // 31-09-2018

-(void) setTD1NumPages:(int)num_pages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(int) getTD1NumPages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setTD2NumPages:(int)num_pages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(int) getTD2NumPages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setTD3NumPages:(int)num_pages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(int) getTD3NumPages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setCustomNumPages:(int)num_pages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(int) getCustomNumPages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(void) setFallbackNumPages:(int)num_pages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018
-(int) getFallbackNumPages __deprecated_msg("Use setCaptureDocuments instead."); // 31-09-2018

-(void) displayMrzZone:(bool)show;
-(void) setDetectionZoneMrzBackgroundColor:(UIColor*)color;
-(void) setDetectionZoneMrzBackgroundHeight:(double)height;
-(void) setDetectionZoneMrzBackgroundAlpha:(double)alpha;

-(bool) isRunningOnCompatibleArch;

-(NSArray*) getCropPoints;


@end
