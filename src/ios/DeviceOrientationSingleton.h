//
//  DeviceOrientationSingleton.h
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#ifndef DeviceOrientationSingleton_h
#define DeviceOrientationSingleton_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DeviceOrientationSingleton : NSObject {
    UIDeviceOrientation deviceOrientation;
}

+ (id)sharedManager;

- (void) resumeOrientationUpdates;
- (void) stopOrientationUpdates;
- (UIInterfaceOrientation) interfaceOrientation;

@end

#endif /* DeviceOrientationSingleton_h */
